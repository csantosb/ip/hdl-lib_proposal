;;; activate org-reveal
(require 'ox-reveal)

;;; activate htmlize
(require 'htmlize)

;;; export
(with-current-buffer (find-file-noselect "hdl-library-management.org")
  (org-reveal-export-to-html))
